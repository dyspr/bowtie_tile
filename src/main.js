var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var dimension = 7
var frame = 0
var rotation = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
      rotate(((i + j) % 2) * Math.PI * 0.5 + frame * 0.5)
      drawTile(boardSize * initSize * 0.9, 255, frame)
      if ((mouseX / windowWidth) >= 0.5) {
        fill(0)
        noStroke()
        rect(0, 0, boardSize * initSize * 0.25, boardSize * initSize * 0.25)
      }
      pop()
    }
  }

  frame += deltaTime * 0.0005
  if (frame > 2 * Math.PI) {
    frame = 0
    rotation++
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawTile(size, col, state) {
  fill(col)
  stroke(col)
  strokeWeight(1)
  beginShape()
  vertex(-size * 0.5, size * 0.25 + sin(Math.PI + state) * size * 0.25)
  vertex(-size * 0.5, -size * 0.25 + sin(state) * size * 0.25)
  vertex(-size * 0.125, -size * 0.125)
  vertex(size * 0.125, -size * 0.125)
  vertex(size * 0.5, -size * 0.25 + sin(state) * size * 0.25)
  vertex(size * 0.5, size * 0.25 + sin(Math.PI + state) * size * 0.25)
  vertex(size * 0.125, size * 0.125)
  vertex(-size * 0.125, size * 0.125)
  endShape()
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
